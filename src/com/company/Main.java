package com.company;
import java.util.ArrayList;
public class Main {

    public static void main(String[] args) {
        ArrayList<MedicalService> commodityArrayList = new ArrayList<>();
        commodityArrayList.add(new ScheduledInspection("  Городская поликлиника №12", "  Улица Маркинская, дом 45", "  Хорошилова", "  78452-12358", "  12.12.2020",
                "  Макарова", "  Терапевт", "  ОРВИ", "  Стационарный", "  2020", "  С 12.12.2020 по 14.12.2020", "  Пациент болеет ОРВИ"));
        commodityArrayList.add(new Vaccination("  Детская поликлиника №20", "  Улица Лихачёва, дом 21", "  Меньшикова", "  74152-95523", "  07.09.2020",
                "  Морозова", "  Эдокринолог", "  ГРИПП", "  Амбулаторный", "  ГРИПП-2020", "  07.09.2020 до 07.09.2021"));
        commodityArrayList.add(new MedicalCareForChildrenAndAdolescents("  Облостная детская больница №1", "  Улица Дорожная, дом 17", "  Смотров", "  75268-02182", "  17.10.2020",
                "  Полякова", "  Хирург", "  Пиелонефрит", "  Смотров Владимир Сергеевич, 18.08.2015", "  Мужской", "  5 лет"));

        for (MedicalService com : commodityArrayList) {
            System.out.println(com.toString());

        }
    }
}

