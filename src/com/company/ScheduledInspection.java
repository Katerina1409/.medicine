package com.company;

public class ScheduledInspection extends MedicalService {
    private String view;
    private String yearHolding;
    private String period;
    private String result;

    public ScheduledInspection(String name, String address, String surname, String number, String date, String surnameDoctor, String position, String diagnosis, String view, String yearHolding, String period, String result) {
        super(name, address, surname, number, date, surnameDoctor, position, diagnosis);
        this.view = view;
        this.yearHolding = yearHolding;
        this.period = period;
        this.result = result;
    }

    @Override
    public String toString() {
        return super.toString() + "\n" +
                "view =" + view + "\n" +
                "yearHolding =" + yearHolding + "\n" +
                "period =" + period + "\n" +
                "result =" + result;

    }
}
