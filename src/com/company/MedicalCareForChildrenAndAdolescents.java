package com.company;

public class MedicalCareForChildrenAndAdolescents extends MedicalService {
    private String birthCertificate;
    private String gender;
    private String age;

    public MedicalCareForChildrenAndAdolescents(String name, String address, String surname, String number, String date, String surnameDoctor, String position, String diagnosis, String birthCertificate, String gender, String age) {
        super(name, address, surname, number, date, surnameDoctor, position, diagnosis);
        this.birthCertificate = birthCertificate;
        this.gender = gender;
        this.age = age;
    }

    @Override
    public String toString() {
        return
                super.toString() + "\n" +
                        "birthCertificate =" + birthCertificate + "\n" +
                        "gender =" + gender + "\n" +
                        "age =" + age;
    }
}
