package com.company;

public class MedicalService {
    private String name;
    private String address;
    private String surname;
    private String number;
    private String date;
    private String surnameDoctor;
    private String position;
    private String diagnosis;

    public MedicalService(String name, String address, String surname, String number, String date, String surnameDoctor, String position, String diagnosis) {
        this.name = name;
        this.address = address;
        this.surname = surname;
        this.number = number;
        this.date = date;
        this.surnameDoctor = surnameDoctor;
        this.position = position;
        this.diagnosis= diagnosis;
    }
    @Override
    public String toString(){
        return  "name =" + name + "\n" +
                "address =" + address + "\n" +
                "surname =" + surname + "\n" +
                "number =" + number + "\n" +
                "date =" + date + "\n" +
                "surnameDoctor =" + surnameDoctor + "\n" +
                "position =" + position + "\n" +
                "diagnosis =" + diagnosis;

    }
}
