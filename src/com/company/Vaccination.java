package com.company;

public class Vaccination extends MedicalService {
    private String nameVaccine;
    private String dateVaccinations;
    private String period;

    public Vaccination(String name, String address, String surname, String number, String date, String surnameDoctor, String position, String diagnosis, String nameVaccine, String dateVaccinations, String period) {
        super(name, address, surname, number, date, surnameDoctor, position, diagnosis);
        this.nameVaccine = nameVaccine;
        this.dateVaccinations = dateVaccinations;
        this.period = period;
    }

    @Override
    public String toString() {
        return
                super.toString() + "\n" +
                        "nameVaccine =" + nameVaccine + "\n" +
                        "dateVaccinations =" + dateVaccinations + "\n" +
                        "period =" + period;
    }
}
